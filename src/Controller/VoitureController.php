<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Repository\VoitureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogue/voitures/", name="catalogue_voitures_")
 */
class VoitureController extends AbstractController
{
    private $voitureRepo;

    public function __construct(VoitureRepository $voitureRepository) {
        $this->voitureRepo = $voitureRepository;
    }

    /**
     * @Route("{marque}/{modele}", name="show_voiture")
     */
    public function showVoiture($marque, $modele) {
        $voiture = $this->voitureRepo->findOneBy(["modele" => $modele]);

        return $this->render("voitures/show.html.twig", [
            "voiture" => $voiture,
            "metaDesc" => "Page de la voiture {{ voiture.marque }} {{ voiture.modele }} avec ses caractéristiques et son prix."
        ]);
    }
}