<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\VoitureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="index_")
 */
class IndexController extends AbstractController
{
    private $articleRepo;
    private $voitureRepo;

    public function __construct(ArticleRepository $articleRepo, VoitureRepository $voitureRepo)
    {
        $this->articleRepo = $articleRepo;
        $this->voitureRepo = $voitureRepo;
    }

    /**
     * @Route("", name="homepage")
     */
    public function index()
    {
        $articles = $this->articleRepo->findLast4();
        $voitures = $this->voitureRepo->findAll();

        return $this->render("homepage.html.twig", [
            "articles" => $articles,
            "voitures" => $voitures,
            "metaDesc" => "Bienvenue sur la page d'accueil de notre site web de vente de voitures Mercedes avec les meilleurs modèles."
        ]);
    }

}