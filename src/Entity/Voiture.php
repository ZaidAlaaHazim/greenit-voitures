<?php

namespace App\Entity;

use App\Repository\VoitureRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VoitureRepository::class)]
class Voiture
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $marque = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $modele = null;

    #[ORM\Column(nullable: true)]
    private ?float $prix = null;

    #[ORM\Column(type: 'string')]
    private $imageFileName;

    #[ORM\Column(nullable: true)]
    private ?float $chevaux = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typeEssence = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $boiteVitesse = null;

    public function getImageFileName()
    {
        return $this->imageFileName;
    }

    public function setImageFileName($imageFilename)
    {
        $this->imageFileName = $imageFilename;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(?string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(?string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getChevaux(): ?float
    {
        return $this->chevaux;
    }

    public function setChevaux(?float $chevaux): self
    {
        $this->chevaux = $chevaux;

        return $this;
    }

    public function getTypeEssence(): ?string
    {
        return $this->typeEssence;
    }

    public function setTypeEssence(?string $typeEssence): self
    {
        $this->typeEssence = $typeEssence;

        return $this;
    }

    public function getBoiteVitesse(): ?string
    {
        return $this->boiteVitesse;
    }

    public function setBoiteVitesse(?string $boiteVitesse): self
    {
        $this->boiteVitesse = $boiteVitesse;

        return $this;
    }
}
